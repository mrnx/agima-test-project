import json
import re
from collections import OrderedDict
from html import escape


class ConverterException(Exception):
    pass


class Converter:
    """
    Класс для конвертации JSON файлов в различные

    :param filename: имя json файла для чтения
    :param ul_li_mode: режим HTML списков
    :param escape_mode: режим экранирования HTML разметки
    :param old_mode: режим совместимости с Задачей #1
    """

    def __init__(self, filename=None, ul_li_mode=False, escape_mode=False, old_mode=False):
        self.filename = filename
        self.data = None
        self.ul_li_mode = ul_li_mode
        self.escape_mode = escape_mode
        self.old_mode = old_mode

    def read_data(self):
        """Считывает и декодирует данные из JSON файла"""
        with open(self.filename) as f:
            self.data = json.load(f, object_pairs_hook=OrderedDict)

    def convert(self):
        """Конвертирует JSON файл в нужный формат"""
        if self.filename is not None:
            self.read_data()

        return self.render()

    def render(self):
        """Рендерит данные по требованию задачи"""
        src = self.data

        if self.data is None:
            raise ConverterException('No data loaded')

        if isinstance(src, OrderedDict):  # Если JSON не содержит list
            self.ul_li_mode = False
            src = [src]

        return self._rend_recursive(src)

    def _rend_recursive(self, data, ul_li=False):
        """Рекурсивно преобразует JSON в нужный HTML"""
        res = []

        for obj in data:  # Обходим list
            temp = []

            for it in obj.items():  # Обходим object
                if self.old_mode:  # Режим совместимости с Задачей #1
                    if it[0] == 'title':
                        el = {'begin': 'h1', 'end': 'h1'}
                    if it[0] == 'body':
                        el = {'begin': 'p', 'end': 'p'}
                else:
                    el = self._make_tag(it[0])

                # Вызываем рекурсивно, если есть вложенные элементы
                val = self._rend_recursive(it[1], True) if isinstance(it[1], list) else it[1]

                if self.escape_mode:
                    val = escape(val)

                temp.append('<{0}>{1}</{2}>'.format(el['begin'], val, el['end']))

            res.append(''.join(temp))

        if self.ul_li_mode or ul_li:
            return self._to_ul_li(res)
        else:
            return ''.join(res)

    @staticmethod
    def _to_ul_li(items):
        """Преобразует list в HTML список"""
        return '<ul><li>' + '</li><li>'.join(items) + '</li></ul>'

    @staticmethod
    def _parse_selectors(selector) -> dict:
        """Разбирает CSS селекторы следующего вида: element.class1.classN#id"""
        sel = re.compile(r'^(?P<tag>[\w]+)(?P<classes>\.[\w\-.]+)?(?P<id>#[\w\-]+)?$')
        res = {'tag': None, 'classes': None, 'id': None}

        parsed = sel.match(selector).groupdict()
        res['tag'] = parsed['tag']

        if parsed['id'] is not None:
            res['id'] = parsed['id'][1:]

        if parsed['classes'] is not None:
            res['classes'] = parsed['classes'].split('.')[1:]

        return res

    def _make_tag(self, selector):
        """Генерирует HTML-тег по разобранному селектору"""
        parced = self._parse_selectors(selector)

        tag = parced['tag']
        classes = ''
        id_ = ''

        if parced['classes'] is not None:
            classes = ' class="' + ' '.join(parced['classes']) + '"'

        if parced['id'] is not None:
            id_ = ' id="' + parced['id'] + '"'

        begin = tag + id_ + classes

        return {'begin': begin, 'end': tag}


def get_data(filename):
    """Deprecated"""
    conv = Converter(filename)
    conv.read_data()
    return conv.data


def render(data):
    """Deprecated"""
    conv = Converter()
    conv.data = data

    if len(data) > 0 and 'title' in data[0] and 'body' in data[1]:  # Пробуем определить формат Задачи #1
        conv.old_mode = True

    return conv.render()


if __name__ == '__main__':
    JSON_FILE = 'source.json'

    try:
        conv = Converter(JSON_FILE, ul_li_mode=True, escape_mode=True)
        print(conv.convert())
    except ConverterException as e:
        print('Some error: {0}'.format(e))
        exit(1)
