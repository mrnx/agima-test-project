import unittest
import converter
from collections import OrderedDict


class ConverterTest(unittest.TestCase):
    """Тестируем функцию рендера для всех задач"""

    def test_task_1(self):
        """Задача #1"""
        before = [
            OrderedDict([('title', 'Title #1'), ('body', 'Hello, World 1!')]),
            OrderedDict([('title', 'Title #2'), ('body', 'Hello, World 2!')])
        ]
        after = '<h1>Title #1</h1><p>Hello, World 1!</p><h1>Title #2</h1><p>Hello, World 2!</p>'

        conv = converter.Converter(old_mode=True)
        conv.data = before

        self.assertEqual(conv.render(), after)

    def test_task_1_old(self):
        """Задача #1 (устаревший API)"""
        before = [
            OrderedDict([('title', 'Title #1'), ('body', 'Hello, World 1!')]),
            OrderedDict([('title', 'Title #2'), ('body', 'Hello, World 2!')])
        ]
        after = '<h1>Title #1</h1><p>Hello, World 1!</p><h1>Title #2</h1><p>Hello, World 2!</p>'

        self.assertEqual(converter.render(before), after)

    def test_task_2(self):
        """Задача #2"""
        before = [OrderedDict([('h3', 'Title #1'), ('div', 'Hello, World 1!')])]
        after = '<h3>Title #1</h3><div>Hello, World 1!</div>'

        conv = converter.Converter()
        conv.data = before

        self.assertEqual(conv.render(), after)

    def test_task_2_old(self):
        """Задача #2 (Устаревший API)"""
        before = [OrderedDict([('h3', 'Title #1'), ('div', 'Hello, World 1!')])]
        after = '<h3>Title #1</h3><div>Hello, World 1!</div>'

        self.assertEqual(converter.render(before), after)

    def test_task_3(self):
        """Задача #3"""
        before = [
            OrderedDict([('h3', 'Title #1'), ('div', 'Hello, World 1!')]),
            OrderedDict([('h3', 'Title #2'), ('div', 'Hello, World 2!')])
        ]
        after = '<ul><li><h3>Title #1</h3><div>Hello, World 1!</div></li>' \
                '<li><h3>Title #2</h3><div>Hello, World 2!</div></li></ul>'

        conv = converter.Converter(ul_li_mode=True)
        conv.data = before

        self.assertEqual(conv.render(), after)

    def test_task_4_1(self):
        """Задача #4, пример 1"""
        before = OrderedDict([('p', 'hello1')])
        after = '<p>hello1</p>'

        conv = converter.Converter(ul_li_mode=True)
        conv.data = before

        self.assertEqual(conv.render(), after)

    def test_task_4_2(self):
        """Задача #4, пример 2"""
        before = [
            OrderedDict([
                ('span', 'Title #1'),
                ('content', [OrderedDict([('p', 'Example 1'), ('header', 'header 1')])])
            ]),
            OrderedDict([('div', 'div 1')])
        ]
        after = '<ul><li><span>Title #1</span><content><ul><li><p>Example 1</p>' \
                '<header>header 1</header></li></ul></content></li><li><div>div 1</div></li></ul>'

        conv = converter.Converter(ul_li_mode=True)
        conv.data = before

        self.assertEqual(conv.render(), after)

    def test_task_5(self):
        """Задача #5"""
        before = OrderedDict([('p.my-class#my-id', 'hello'), ('p.my-class1.my-class2', 'example<a>asd</a>')])
        after = '<p id="my-id" class="my-class">hello</p>' \
                '<p class="my-class1 my-class2">example&lt;a&gt;asd&lt;/a&gt;</p>'

        conv = converter.Converter(ul_li_mode=True, escape_mode=True)
        conv.data = before

        self.assertEqual(conv.render(), after)


if __name__ == '__main__':
    unittest.main()
